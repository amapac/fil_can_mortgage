class CreateQuickAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :quick_appointments do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :phone
      t.string :time
      t.string :loan_amt

      t.timestamps
    end
  end
end
