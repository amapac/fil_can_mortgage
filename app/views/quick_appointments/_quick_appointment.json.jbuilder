json.extract! quick_appointment, :id, :firstname, :lastname, :email, :phone, :time, :loan_amt, :created_at, :updated_at
json.url quick_appointment_url(quick_appointment, format: :json)
