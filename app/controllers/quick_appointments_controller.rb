class QuickAppointmentsController < ApplicationController
  before_action :set_quick_appointment, only: [:show, :edit, :update, :destroy]

  # GET /quick_appointments
  # GET /quick_appointments.json
  def index
    @quick_appointments = QuickAppointment.all
  end

  # GET /quick_appointments/1
  # GET /quick_appointments/1.json
  def show
  end

  # GET /quick_appointments/new
  def new
    @quick_appointment = QuickAppointment.new
  end

  # GET /quick_appointments/1/edit
  def edit
  end

  # POST /quick_appointments
  # POST /quick_appointments.json
  def create
    @quick_appointment = QuickAppointment.new(quick_appointment_params)

    respond_to do |format|
      if @quick_appointment.save
        format.html { redirect_to @quick_appointment, notice: 'Quick appointment was successfully created.' }
        format.json { render :show, status: :created, location: @quick_appointment }
      else
        format.html { render :new }
        format.json { render json: @quick_appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quick_appointments/1
  # PATCH/PUT /quick_appointments/1.json
  def update
    respond_to do |format|
      if @quick_appointment.update(quick_appointment_params)
        format.html { redirect_to @quick_appointment, notice: 'Quick appointment was successfully updated.' }
        format.json { render :show, status: :ok, location: @quick_appointment }
      else
        format.html { render :edit }
        format.json { render json: @quick_appointment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quick_appointments/1
  # DELETE /quick_appointments/1.json
  def destroy
    @quick_appointment.destroy
    respond_to do |format|
      format.html { redirect_to quick_appointments_url, notice: 'Quick appointment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quick_appointment
      @quick_appointment = QuickAppointment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quick_appointment_params
      params.require(:quick_appointment).permit(:firstname, :lastname, :email, :phone, :time, :loan_amt)
    end
end
