require 'test_helper'

class QuickAppointmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @quick_appointment = quick_appointments(:one)
  end

  test "should get index" do
    get quick_appointments_url
    assert_response :success
  end

  test "should get new" do
    get new_quick_appointment_url
    assert_response :success
  end

  test "should create quick_appointment" do
    assert_difference('QuickAppointment.count') do
      post quick_appointments_url, params: { quick_appointment: { email: @quick_appointment.email, firstname: @quick_appointment.firstname, lastname: @quick_appointment.lastname, loan_amt: @quick_appointment.loan_amt, phone: @quick_appointment.phone, time: @quick_appointment.time } }
    end

    assert_redirected_to quick_appointment_url(QuickAppointment.last)
  end

  test "should show quick_appointment" do
    get quick_appointment_url(@quick_appointment)
    assert_response :success
  end

  test "should get edit" do
    get edit_quick_appointment_url(@quick_appointment)
    assert_response :success
  end

  test "should update quick_appointment" do
    patch quick_appointment_url(@quick_appointment), params: { quick_appointment: { email: @quick_appointment.email, firstname: @quick_appointment.firstname, lastname: @quick_appointment.lastname, loan_amt: @quick_appointment.loan_amt, phone: @quick_appointment.phone, time: @quick_appointment.time } }
    assert_redirected_to quick_appointment_url(@quick_appointment)
  end

  test "should destroy quick_appointment" do
    assert_difference('QuickAppointment.count', -1) do
      delete quick_appointment_url(@quick_appointment)
    end

    assert_redirected_to quick_appointments_url
  end
end
